datos <- read.table('detergente.txt', header = T)
#1.1
#realicemos el analisis descriptivo:
nA = length(datos$A) # tamano de la muestra 
summary(datos$A)
sdA <- sd(datos$A)
hist(datos$A, main='Sustancia A', ylab='Frecuencia', xlab='Tiempo en eliminar la grasa')
boxplot(datos$A, main='Tiempo en eliminar la grasa para A')

B = na.omit(datos$B)
nB = length(datos$B) # tamano de la muestra
summary(datos$B)
sdB <- sd(B)
hist(datos$B, main='Sustancia B', ylab='Frecuencia', xlab='Tiempo en eliminar la grasa')
boxplot(datos$B, main='Tiempo en eliminar la grasa para B')

#1.2
TpA <- (mean(datos$A)-38 / (sdA/sqrt(nA)))
pvalorA <- pt(TpA, nA-1)
t.test(datos$A, alternative="less", mu=38, conf.level = 0.99)

#1.3

TpB <- (mean(B)-44 / (sdA/sqrt(nB)))
pvalorB <- pt(TpB, nB-1)
t.test(B, alternative="two.sided", mu=44, conf.level = 0.95)

#2.1

alpha <- 0.05; n <- 150; k <- 6 ; r <- 0 
p <- c(1/6, 1/6, 1/6, 1/6, 1/6, 1/6)
fi <- c(22, 21, 22, 27, 22, 36)
chi2_obs <- sum((fi-n*p)^2/(n*p))
chi2_alpha <- qchisq(1-alpha, k-1-r)
p_valor <- 1 - pchisq(chi2_obs, k-1-r)
prop.test(fi, c(150, 150, 150, 150, 150, 150), p)
#2.2

alpha <- 0.05
r <- 2
fi <- c(16, 18, 22, 51, 62, 55, 22, 4)
qqnorm(fi)
qqline(fi)

k <- length(fi)

n <- sum(fi)
mi <- c(35, 45, 55, 65, 75, 85, 95, 105)
xbarra <- sum(fi*mi)/n
x_barra <- rep(xbarra, k)
S_cuadrado <- sum(fi*(mi-x_barra)^2)/(n-1)
S <- sqrt(S_cuadrado)
pi <- pnorm(4:11*10, xbarra, S) - pnorm(3:10*10, xbarra, S)
chi2_obs <- sum((fi-n*pi)^2/(n*pi))
chi2_alpha <- qchisq(1-alpha, k-1-r)
p_valor <- 1 - pchisq(chi2_obs, k-1-r)
p_valor
