rm(list=ls())
dev.off()
datos <- read.table('caracteristicasfisicas.txt', header = T)
prueba <- read.table('Caracteristicasfisicas_prediccion.txt', header = T)


# 1.

k = 1
par(mfrow=c(1,2), mar=c(4,4,4,4))
for (name in names(datos[, !(names(datos) %in% c('zona'))])) {
  if (name != 'zona') {
    print(paste('Datos de ', name, sep=''))
    hist(datos[[name]], main=name, xlab=name, ylab='Densidad')
    print(summary(datos[[name]]))
    print(paste('desviacion estandar:', sd(datos[[name]])))
    k = k + 1 
    boxplot(datos[[name]], main=name)
  }
}

# 2.

pairs(datos[, !(names(datos) %in% c('zona'))])
cor(datos[, !(names(datos) %in% c('zona'))])

# 3. 

alpha <- 0.05
r <- 2
h <- hist(datos$estatura)
fi <- h$counts
k <- length(fi)
n <- sum(fi)
mi <- h$mids
xbarra <- sum(fi*mi)/n
x_barra <- rep(xbarra, k)
S_cuadrado <- sum( fi*(mi-x_barra)^2 )/(n-1)
S <- sqrt(S_cuadrado)
pi <- pnorm(h$breaks[2:length(h$breaks)], xbarra, S) - pnorm(h$breaks[1:length(h$breaks)-1], xbarra, S)
chi2_obs <- sum((fi-n*pi)^2/(n*pi))
chi2_alpha <- qchisq(1 - alpha, k - 1 -r)
p_valor <- 1 - pchisq(chi2_obs, k - 1 - r)


# 4.

m1 = lm(estatura~peso+pie+lbrazo+anchoes+dcraneo+lromedtob+IMC, data=datos)
summary(m1)
m2 =lm(estatura~peso+pie+lbrazo+dcraneo+IMC, data=datos)
summary(m2)
m3=lm(estatura~peso+pie+IMC,data=datos)
summary(m3)

plot(m3)



# 5.

Datos_observados = prueba[1]
Datos_estimados = predict(m3, newdata=prueba[c('peso','pie','IMC')])
Residuos = Datos_observados - Datos_estimados
hist(sapply(Residuos,as.numeric), main='Histograma de residuos',xlab="Residuos")
boxplot(sapply(Residuos,as.numeric),main='Boxplot de residuos', xlab="Residuos")


# 6.

IMC_cat = c()
k = 1
for (imc in datos$IMC) {
  if (imc < 16) {
    IMC_cat[k] = 1
  } else if (16 <= imc && imc <= 16.99) {
    IMC_cat[k] = 2
  } else if (17 <= imc && imc <= 18.49) {
    IMC_cat[k] = 3
  } else if (18.5 <= imc && imc <= 24.99) {
    IMC_cat[k] = 4
  } else if (25 <= imc && imc <= 29.99) {
    IMC_cat[k] = 5
  } else if (30 <= imc && imc <= 34.99) {
    IMC_cat[k] = 6
  } else if (35 <= imc && imc <= 39.99) {
    IMC_cat[k] = 7
  } else if (40 <= imc && imc <= 44.99) {
    IMC_cat[k] = 8
  } else {
    IMC_cat[k] = 9
  }
  k = k+1
}

datos$IMC_cat <- IMC_cat

# 7.

zA <- datos[datos$zona=="A",]$estatura
zB <- datos[datos$zona=="B",]$estatura
zC <- datos[datos$zona=="C",]$estatura
dat1 <- c(zA, zB, zC)
fac1 <- c(replicate(length(zA), 'zA'), replicate(length(zB), 'zB'), replicate(length(zC), 'zC'))
fact1 = factor(fac1)

i1 <- datos[datos$IMC_cat==1,]$estatura
i2 <- datos[datos$IMC_cat==2,]$estatura
i3 <- datos[datos$IMC_cat==3,]$estatura
i4 <- datos[datos$IMC_cat==4,]$estatura
i5 <- datos[datos$IMC_cat==5,]$estatura
i6 <- datos[datos$IMC_cat==6,]$estatura
i7 <- datos[datos$IMC_cat==7,]$estatura
dat2 <- c(i1, i2, i3, i4, i5, i6, i7)
fac2 <- c(replicate(length(i1), 'i1'), replicate(length(i2), 'i2'), replicate(length(i3), 'i3'),
          replicate(length(i4), 'i4'), replicate(length(i5), 'i5'), replicate(length(i6), 'i6'),
          replicate(length(i7), 'i7'))
fact2 <- factor(fac2)


tapply(dat1,fact1,mean)
tapply(dat2,fact2,mean)



# 8.
mod.lm = lm(dat1~fact1)
anova(mod.lm)

mod.lm = lm(dat2~fact2)
anova(mod.lm)


# 9.
pairwise.t.test(dat2,fact2)

